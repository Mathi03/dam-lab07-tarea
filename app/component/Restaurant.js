import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
  Constants,
} from 'react-native';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from 'react-native-responsive-dimensions';

export default class Restaurant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reutaurantList: [],
    };
    //this.templateItem.bind(this);
  }

  templateItem({item}) {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => {
          this.props.navigation.navigate('Second', {id: item._id});
        }}>
        <Image
          source={{
            uri: `https://prueba-lab8.herokuapp.com/public/images/${item.imagePath}`,
          }}
          style={styles.imageItem}></Image>
        <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.name}</Text>
      </TouchableOpacity>
    );
  }

  async componentDidMount() {
    try {
      const restaurantApi = await fetch(
        'https://prueba-lab8.herokuapp.com/api/restaurante',
      );
      const restaurant = await restaurantApi.json();
      this.setState({reutaurantList: restaurant});
    } catch (err) {
      console.log('Error fetching data-----------', err);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{alignSelf: 'center', fontSize: 25, fontWeight: 'bold'}}>
          Restaurantes
        </Text>
        <FlatList
          data={this.state.reutaurantList}
          renderItem={this.templateItem.bind(this)}
          numColumns={2}
          keyExtractor={(item) => item._id.toString()}></FlatList>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {},
  item: {
    backgroundColor: '#add8e6',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 5,
    padding: 10,
    borderRadius: 10,
  },
  imageItem: {
    height: 150,
    width: 150,
    borderRadius: 10,
  },
});
